package com.hcl.assignment03;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Complete the function power in class MyCalculator and return the
 *         appropriate result after the power operation or an appropriate
 *         exception.
 */
import java.util.Scanner;

public class MyCalculatorMainClass {

	public static void main(String[] args) {

		// Service Class Object creation.
		MyCalculator myCalculator = new MyCalculator();

		// Prompt the user.
		System.out.println("Enter two numbers to calculate power: ");

		// Scanner Object.
		Scanner input = new Scanner(System.in);

		// reading dynamic data and storing in variables.
		int num1 = input.nextInt();

		int num2 = input.nextInt();

		// catching the custom exception.
		try {
			long ans = myCalculator.power(num1, num2);
			System.out.println("The power of " + num1 + " and " + num2 + " : " + ans);
		} catch (InvalidParameterException e) {
			System.err.println("java.lang.Exception" + " : " + e.getMessage());
		}

		// closing costly resources
		input.close();

		// de-referencing
		myCalculator = null;

	}

}
