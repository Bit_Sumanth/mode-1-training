package com.hcl.assignment03;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Complete the function power in class MyCalculator and return the
 *         appropriate result after the power operation or an appropriate
 *         exception.
 */

public class MyCalculator {

	// Throwing the custom Exception. (Business Logic)
	public long power(int num1, int num2) throws InvalidParameterException {

		if (num1 == 0 && num2 == 0) {
			throw new InvalidParameterException("num1 and num2 should not be zero.");
		} else if (num1 < 0 || num2 < 0) {
			throw new InvalidParameterException("num1 and num2 should not be negative.");
		} else // casting the double result to long for business need.
			return (long) Math.pow(num1, num2);

	}
}
