package com.hcl.assignment03;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Complete the function power in class MyCalculator and return the
 *         appropriate result after the power operation or an appropriate
 *         exception.
 */

// Creating Custome Checked Exception.
public class InvalidParameterException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// private data field.
	private String message;

	// arg-constructor
	public InvalidParameterException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

}
