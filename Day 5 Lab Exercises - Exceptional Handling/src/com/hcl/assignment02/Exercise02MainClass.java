package com.hcl.assignment02;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Sumanth Babu.
 * 
 * 
 *         How to Handle the 'InputMismatchException' and
 *         'ArithmeticException'.?
 * 
 *         Eg: To take inputs from user and perform divison operation.
 */
public class Exercise02MainClass {

	public static void main(String[] args) {

		// Scanner Object for Dynamic Data usage.
		Scanner scanner = new Scanner(System.in);

		double ans = 0;

		/*
		 * Both InputMismatchException and ArithmeticException are RuntimeExeptions
		 * 
		 */

		try {
			int num1 = scanner.nextInt();
			int num2 = scanner.nextInt();
			ans = num1 / num2;
		} catch (InputMismatchException ie) {
			System.out.println("java.lang.InputMismatchException");
			ie.getMessage();
		} catch (ArithmeticException ae) {
			System.out.println("java.lang.ArithmeticException : / by zero");
			ae.getMessage();
		}

		System.out.println("Answer: " + ans);

		// closing the costly resources
		scanner.close();

	}

}
