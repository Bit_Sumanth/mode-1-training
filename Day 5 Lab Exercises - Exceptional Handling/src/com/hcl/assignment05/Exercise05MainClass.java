package com.hcl.assignment05;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a program to calculate the run rate with the formula, including
 *         exceptions.
 *         
 *         Use a single catch block.
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Exercise05MainClass {

	public static void main(String[] args) {

		// Reads the input data dynamically
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		// asking input prompt from user.
		System.out.println("Enter the total runs scored ");

		int runsScored = 0;
		int oversFaced = 0;
		float runRate = 0f;

		/**
		 * Parsing is to read the value of one object to convert it to another type.
		 */

		try {
			runsScored = Integer.parseInt(reader.readLine());

			System.out.println("Enter the total overs faced");

			oversFaced = Integer.parseInt(reader.readLine());

			runRate = runsScored / oversFaced;
		} catch (Exception e) {
			// System.out.println( e.getClass().getCanonicalName());
			System.out.println(e.getClass().getName());

		}

		System.out.println("Current Run Rate: " + String.format("%.2f", runRate));

		// de-referencing
		reader = null;
	}

}
