package com.hcl.assignment01;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Handling a checked exception by opening a file.
 */
public class Excercise01MainClass {

	public static void main(String[] args) {

		File myObj;

		try {

			/*
			 * creating file object and specifing the path of file name. If File Name is not
			 * present we get 'FileNotFoundException' we handle this Checked Exception by
			 * respective catch block.
			 */

			myObj = new File(
					"E:\\Java Mode 1 Training\\9thApril-to-31stMay\\Java_Workspace\\Day 5 Lab Exercises - Exceptional Handling\\src\\com\\hcl\\assignment01\\File.txt");

			Scanner myReader = new Scanner(myObj);
			while (myReader.hasNextLine()) { // loop runs till the next line exists, in file.
				String data = myReader.nextLine(); // reading the data.
				System.out.println(data); // printing the output in console.
			}
			myReader.close(); // closing the costly resource(file)
		} catch (FileNotFoundException e) { // catching the exception here!
			System.err.println("Error Ocurred." + e.getMessage());

		}

		// de-referenicing
		myObj = null;

	}

}
