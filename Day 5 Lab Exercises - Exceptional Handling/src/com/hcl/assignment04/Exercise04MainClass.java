package com.hcl.assignment04;

/**
 * 
 * @author Sumanth Babu.
 *	
 *		The player is eligible to participate in IPL when their age is 19 and
 *      above if not create a InvalidAgeRange Custom Exception.
 */
import java.util.Scanner;

public class Exercise04MainClass {

	public static void main(String[] args) {

		// Scanner Object
		Scanner scanner = new Scanner(System.in);

		// dynamic input processing
		System.out.println("Enter the player name");
		String name = scanner.next();

		System.out.println("Enter the player age");
		int age = scanner.nextInt();

		if (age <= 19) {
			try {
				throw new InvalidAgeRangeException("Player " + name + "is Not suitable to Parctcipate in IPL");

			} catch (InvalidAgeRangeException e) {

				System.out.print("Custom Exception : ");

				System.out.print(e.getClass().getCanonicalName());
				// System.err.println(e.getMessage());

			} finally {
				// closing the costly resources.
				scanner.close();
			}
		} else {
			System.out.println("Player Name: " + name);
			System.out.println("Player age: " + age);

		}

		// de-referencing
		scanner = null;
	}

}
