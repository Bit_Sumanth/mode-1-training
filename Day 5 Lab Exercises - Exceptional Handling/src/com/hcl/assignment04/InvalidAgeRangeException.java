package com.hcl.assignment04;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         The player is eligible to participate in IPL when their age is 19 and
 *         above if not create a InvalidAgeRange Custom Exception.
 * 
 */
@SuppressWarnings("serial")
public class InvalidAgeRangeException extends CustomException {

	private String message;// encapsulated.

	public InvalidAgeRangeException(String message) {
		super(message);
		this.message = message;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

}
