package com.hcl.assignment01;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         To Find smallest number among three numbers?
 */
public class Exercise01Main {

	public static void main(String[] args) {

		Exercise01 exercise01 = new Exercise01();

		int ans = exercise01.smallestNumber(76, 22, 30);

		System.out.println("The smallest Number is " + ans);

		exercise01 = null; // de-referencing.

	}

}
