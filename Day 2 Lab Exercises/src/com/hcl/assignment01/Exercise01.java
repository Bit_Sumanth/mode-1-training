package com.hcl.assignment01;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         To Find smallest number among three numbers?
 */
public class Exercise01 {

	// Business Logic
	public int smallestNumber(int num1, int num2, int num3) {

		int smallNo = 0;
		if (num1 < num2 && num1 < num3) {
			smallNo = num1;
		} else if (num2 < num3) {
			smallNo = num2;
		} else {
			smallNo = num3;
		}
		return smallNo;

	}
}
