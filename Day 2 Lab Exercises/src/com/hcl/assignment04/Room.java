package com.hcl.assignment04;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         To create a room class, the attributes of this class is roomno,
 *         roomtype, roomarea and ACmachine. In this class the member functions
 *         are setdata and displaydata.
 */
public class Room {

	private int roomNo;
	private String roomType;
	private double roomArea;
	private String acMachine;

	// default constructor
	public Room() {
		super();
	}

	// Parameterised constructor.
	public Room(int roomNo, String roomType, double roomArea, String acMachine) {
		this.roomNo = roomNo;
		this.roomType = roomType;
		this.roomArea = roomArea;
		this.acMachine = acMachine;
	}

	// getter and setter methods for all fieds.
	public int getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(int roomNo) {
		this.roomNo = roomNo;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public double getRoomArea() {
		return roomArea;
	}

	public void setRoomArea(double roomArea) {
		this.roomArea = roomArea;
	}

	public String getAcMachine() {
		return acMachine;
	}

	public void setAcMachine(String acMachine) {
		this.acMachine = acMachine;
	}

	public void displayRoom() {
		System.out.println("Room no is: " + this.getRoomNo() + ", " + "Room Type is: " + this.getRoomType() + ", "
				+ "Room Area is: " + this.getRoomArea() + " sqft, " + "AC Machine is: " + this.getAcMachine());
	}

}
