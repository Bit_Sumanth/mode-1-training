package com.hcl.assignment04;

/**
 * 
 * @author Sumanth Babu.
 * 
 *
 *         To create a room class, the attributes of this class is roomno,
 *         roomtype, roomarea and ACmachine. In this class the member functions
 *         are setdata and displaydata.
 */
public class RoomMainClass {

	public static void main(String[] args) {

		// setting data by setters methods of each field.
		Room firstRoom = new Room();
		firstRoom.setRoomNo(1);
		firstRoom.setRoomType("Living Room");
		firstRoom.setRoomArea(330);
		firstRoom.setAcMachine("LG");
		firstRoom.displayRoom();

		// setting field data by constructor.
		Room secondRoom = new Room(2, "Dinning Room", 440, "Blue Star");
		secondRoom.displayRoom();

		Room thirdRoom = new Room(3, "Bed Room", 350, "Panasonic");
		thirdRoom.displayRoom();

		// de-referencing
		firstRoom = null;
		secondRoom = null;
		thirdRoom = null;
	}

}
