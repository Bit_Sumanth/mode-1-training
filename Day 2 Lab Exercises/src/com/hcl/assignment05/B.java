package com.hcl.assignment05;

/**
 * 
 * @author Sumanth Babu.
 * 
 * 		Create class named as �A� and create a sub class �B�. Which is extends from class �A�. And use these classes in �inherit� class. 
 *
 */
public class B extends A {
		
		@Override
		public void display(){
			System.out.println("From Class B");
		}
}
