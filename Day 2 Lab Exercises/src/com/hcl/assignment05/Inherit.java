package com.hcl.assignment05;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Create class named as �A� and create a sub class �B�. Which is
 *         extends from class �A�. And use these classes in �inherit� class.
 *
 */
public class Inherit {

	public static void main(String[] args) {

		A a = new A();
		B b = new B();
		a.display();// from A class
		b.display();// from B class
	}

}
