package com.hcl.assignment03;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         To count vowels in a String.
 *
 */
public class Exercise03 {

	// I used byte as return datatype for memory management.
	public byte countOfVowels(String string) {
		String lowerString = string.toLowerCase();
		char[] stringArray = lowerString.toCharArray();
		byte noOfVowels = 0;
		for (char element : stringArray) {
			if (element == 'a' || element == 'e' || element == 'i' || element == 'o' || element == 'u') {
				++noOfVowels;
			}
		}
		return noOfVowels;

	}
}
