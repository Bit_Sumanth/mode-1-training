package com.hcl.assignment03;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         To count vowels in a String.
 */
public class Exercise03Main {

	public static void main(String[] args) {

		Exercise03 exercise03 = new Exercise03();

		byte ans = exercise03.countOfVowels("HCL Technologies");

		System.out.println("The number of Vowels in a String is = " + ans);

		exercise03 = null; // de-referincing.

	}

}
