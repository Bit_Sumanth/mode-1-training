package com.hcl.assignment09;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         output of the following code.
 */
class Child1 extends Parent { // Inheritence
}

class Child2 extends Parent { // Inheritence
}

public class Test

{

	public static void main(String[] args)

	{
		// objects are created for each class.
		Parent p = new Parent();

		Child1 c1 = new Child1();

		Child2 c2 = new Child2();

		/**
		 * 'Instanceof' operator is used to test whether the object is an instance of
		 * the specified type (class or subclass or interface).
		 */

		System.out.println(c1 instanceof Parent);

		System.out.println(c2 instanceof Parent);

		System.out.println(p instanceof Child1);

		System.out.println(p instanceof Child2);

		p = c1;

		System.out.println(p instanceof Child1);

		System.out.println(p instanceof Child2);

		p = c2;

		System.out.println(p instanceof Child1);

		System.out.println(p instanceof Child2);

	}

}
