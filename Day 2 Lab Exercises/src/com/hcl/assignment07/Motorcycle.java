package com.hcl.assignment07;
/**
 * 
 * @author Sumanth Babu.
 *		
 *		Modify the code to get output?
 */
public class Motorcycle extends Cycle{
	
	String define_me(){ 

        return "a cycle with an engine."; 

    } 

     

    Motorcycle(){ 

        System.out.println("Hello I am a Motorcycle I am "+ define_me()); 

        String temp=super.define_me(); 

        System.out.println("My ancestor is a cycle who is "+ temp ); 

    } 
}
