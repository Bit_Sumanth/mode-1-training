package com.hcl.assignment07;

/**
 * 
 * @author Sumanth Babu.
 *		
 *		Modify the code to get output?
 */
public class InheritenceExample {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		// Bike M = new Bike();
		Motorcycle motorcycle = new Motorcycle();

		motorcycle = null; // de-referencing
	}

}
