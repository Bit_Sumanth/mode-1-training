package com.hcl.assignment08;

public class Dog extends Animal { // Inheritence

	// Behaviours of Dog object.

	void eat() {

		System.out.println("I am eating");

	}

	void bark() {
		System.out.println("I am barking");
	}
}
