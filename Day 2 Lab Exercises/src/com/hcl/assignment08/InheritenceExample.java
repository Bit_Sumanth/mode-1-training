package com.hcl.assignment08;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Modify the code to get output.
 *
 */
public class InheritenceExample {

	public static void main(String[] args) {

		// Object creation == Occupies memory at Heap Area.
		Dog dog = new Dog();

		dog.walk();

		dog.eat();

		dog.bark();

		// de-referencing.
		dog = null;
	}

}
