package com.hcl.assignment06;

/**
 * 
 * @author Sumanth Babu.
 *	
 *			Addition of given numbers.
 */
public class AdditionMain {

	public static void main(String[] args) {
		Addition addition = new Addition();
		int[] numArray = { 1, 2, 3, 4, 5, 6 };
		addition.addTheNumbers(numArray);
	}

}
