package com.hcl.assignment02;

/**
 * 
 * @author Sumanth Babu.
 * 
 * 
 *         Display middle character of a String.
 *
 */
public class Exercise02 {

	public String middleCharacterOfString(String string) {

		if (string.length() % 2 != 0) {
			char[] stringArray = string.toCharArray();
			return stringArray[string.length() / 2] + "";

		} else {
			return string.substring(string.length() / 2 - 1, string.length() / 2 + 1);
		}

	}
}
