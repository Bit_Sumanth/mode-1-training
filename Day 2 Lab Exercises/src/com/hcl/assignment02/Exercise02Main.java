package com.hcl.assignment02;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         To count vowels in a String.
 */
public class Exercise02Main {

	public static void main(String[] args) {

		Exercise02 exercise02 = new Exercise02();

		String ans = exercise02.middleCharacterOfString("ABC");

		System.out.println("The middle character is " + ans);

		exercise02 = null; // de-referencing
	}

}
