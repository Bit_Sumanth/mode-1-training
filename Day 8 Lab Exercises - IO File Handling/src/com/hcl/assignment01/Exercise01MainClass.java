package com.hcl.assignment01;

/**
 * 
 * @author Sumanth Babu.
 *	
 *		using BufferedReader class  to prompt a user to input his/her name and then the output 
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Exercise01MainClass {

	public static void main(String[] args) {

		/**
		 * java.io.BufferedReader class (extends Reader) is used to read the text from a
		 * character-based input stream.
		 * 
		 * 
		 * InputStreamReader is a bridge from byte streams to character streams: It
		 * reads bytes and decodes them into characters using a specified charset.
		 */

		// object creation
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		// prompt the user for input.
		System.out.println("Enter your Name : ");

		String name = null;

		try {
			name = reader.readLine(); // reading a line of text from command line.
		} catch (IOException e) { // handling checked exception.

			System.err.println(e.getMessage());
		}

		System.out.println("Hello " + name); // displays output.

		// de-referencing or unreachable object
		reader = null;
	}

}
