package com.hcl.assignment02;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         How to write a plain Text into File?
 */
public class WriteATextFileMainClass {

	public static void main(String[] args) {

		// Declaring Reference variables to the necessary objects.
		FileWriter writer = null;
		BufferedWriter bufferedWriter = null;

		try {

			// creating filewriter object.
			writer = new FileWriter(
					"E:\\Java Mode 1 Training\\9thApril-to-31stMay\\Java_Workspace\\Day 8 Lab Exercises - IO File Handling\\src\\com\\hcl\\assignment02\\WriteTextFile.txt");

			// buffereader object creation.
			bufferedWriter = new BufferedWriter(writer);

			/*
			 * writing the text into file by using write method from buffereader object.
			 */
			bufferedWriter.write("Hello World");

			bufferedWriter.close();// If not closed then character stream is not written into file.
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

		// de-referencing (now objects are eligible for garbage collection)

		writer = null;
		bufferedWriter = null;

		// A message which indicates the program writes the text to the file mentioned.
		System.out.println("The File is written successfully");
	}

}
