package com.hcl.assignment02;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         How to read a text from File?
 *
 */
public class ReadATextFileMainClass {

	public static void main(String[] args) throws IOException {

		/**
		 * java.io.FileReader is a class for reading text files using the default
		 * character encoding of the operating system.
		 * 
		 * BufferedReader class reads text from a character stream with efficiency.
		 */

		// Example of nested try-catch block.

		java.io.FileReader reader = null;
		BufferedReader bufferedReader = null;
		try {

			// File reader object along with, file path specified.
			// reader = new FileReader(
			// "E:\\Java Mode 1 Training\\9thApril-to-31stMay\\Java_Workspace\\Day 8 Lab
			// Exercises - IO File
			// Handling\\src\\com\\hcl\\assignment02\\ReadTextFile.txt");

			String basePath = new File("").getAbsolutePath();
			String relative = basePath.concat("//src//com//hcl//assignment02//ReadTextFile.txt");
			reader = new FileReader(relative);
			bufferedReader = new BufferedReader(reader);

			String line;
			try {
				// while loop to read the text untill it becomes null value.
				while ((line = bufferedReader.readLine()) != null) {

					System.out.println(line);

				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}

		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		} finally {

			// Here Both Lines Throws IOException which are finally handled by JVM.
			// closing the costly resources gracefully.

			reader.close();
			bufferedReader.close();
		}

	}

}
