package com.hcl.assignment04;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a Java program to read first 3 lines from a file.
 *
 */
public class Exercise04MainClass {

	public static void main(String[] args) {

		FileInputStream fis = null;
		try {
			// the file to be opened for reading
			fis = new FileInputStream(
					"E:\\Java Mode 1 Training\\9thApril-to-31stMay\\Java_Workspace\\Day 8 Lab Exercises - IO File Handling\\src\\com\\hcl\\assignment04\\ReadTextFile.txt");
			Scanner scanner = new Scanner(fis); // file to be scanned
			// returns true if there is another line to read

			// A count variable to check, read only 3 lines from a file.
			int count = 0;
			while (scanner.hasNextLine()) {
				if (count == 3)
					break;
				System.out.println(scanner.nextLine()); // printing lines on to console.
				count++;
			}
			scanner.close(); // closes the scanner
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

		// de-referencing the FileInputStreamObject.
		fis = null;
	}

}
