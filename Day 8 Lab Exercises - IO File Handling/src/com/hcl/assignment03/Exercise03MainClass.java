package com.hcl.assignment03;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a Java program to append text to an existing file.
 */
public class Exercise03MainClass {

	public static void main(String[] args) {

		FileWriter writer = null;// reference variable declaration for future object initialization.
		BufferedWriter bufferedWriter = null;

		try {

			writer = new FileWriter(
					"E:\\Java Mode 1 Training\\9thApril-to-31stMay\\Java_Workspace\\Day 8 Lab Exercises - IO File Handling\\src\\com\\hcl\\assignment03\\WriteTextFile.txt",
					true);// the value 'true' is to set FileWriter object in appender mode.

			bufferedWriter = new BufferedWriter(writer);

			bufferedWriter.write("Hello World");// writing the text into file.

			bufferedWriter.close();// If not closed then character stream is not written into file.
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

		// de-referencing (now objects are eligible for garbage collection)

		writer = null;
		bufferedWriter = null;

		// A message which indicates the program writes the text to the file mentioned.
		System.out.println("The Text is appended to File successfully");

	}

}
