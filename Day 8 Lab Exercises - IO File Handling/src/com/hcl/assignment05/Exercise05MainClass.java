package com.hcl.assignment05;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a Java program to find the longest word in a text file.
 */
import java.io.FileNotFoundException;

public class Exercise05MainClass {

	public static void main(String[] args) {

		// object creation.
		Exercise05 exercise05 = new Exercise05();

		// defining the string variable.
		String longestWord = "";

		try {
			longestWord = exercise05.findLongestWords();
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		}

		// displaying output on the console.
		System.out.println("The Longest Word of the String is : " + longestWord);

		// de-referencing the object to avoid memeory leak problems.
		exercise05 = null;
	}

}
