package com.hcl.assignment05;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a Java program to find the longest word in a text file.
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Exercise05 {

	// business logic == method.
	public String findLongestWords() throws FileNotFoundException {

		String longestWord = "";
		String currentWord;

		// reading the file with scanner object.
		Scanner scanner = new Scanner(new File(
				"E:\\Java Mode 1 Training\\9thApril-to-31stMay\\Java_Workspace\\Day 8 Lab Exercises - IO File Handling\\src\\com\\hcl\\assignment05\\ReadTextFile.txt"));

		while (scanner.hasNext()) {// iterate till nextline in file exists.
			currentWord = scanner.next();
			if (currentWord.length() > longestWord.length()) {
				longestWord = currentWord;
			}

		}
		scanner.close();// closing the costly resource.
		return longestWord;// returning the longest word.
	}

}
