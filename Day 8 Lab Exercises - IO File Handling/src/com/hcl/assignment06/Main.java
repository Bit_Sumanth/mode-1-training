package com.hcl.assignment06;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a java program to record the player details into the file. Get
 *         the player details name, teamName and number of matches played from
 *         the user and write those information in a comma seperated format
 *         (CSV).
 */
public class Main {

	public static void main(String[] args) throws IOException {

		// scanner object instantiation.
		Scanner scanner = new Scanner(System.in);

		// propmt the user for inputs to write into csv file.
		System.out.println("Enter the name of the player ");

		String playerName = scanner.next();// reading values at runtime from console.

		System.out.println("Enter the team name ");

		String teamName = scanner.next();

		System.out.println("Enter the number of matches played ");

		String noOfMatchesPlayed = scanner.next();

		FileOutputStream file = null;

		try {
			file = new FileOutputStream(// file object creation.
					"E:\\Java Mode 1 Training\\9thApril-to-31stMay\\Java_Workspace\\Day 8 Lab Exercises - IO File Handling\\src\\com\\hcl\\assignment06\\player.csv");

			String[] stringArray = new String[3];// string array object to store player details.
			stringArray[0] = playerName;
			stringArray[1] = teamName;
			stringArray[2] = noOfMatchesPlayed;

			file.write(String.join(" , ", stringArray).getBytes());// writing into file by write() method.

			System.out.println("Successfully written in csv file.");

		} catch (IOException e) {
			System.err.println(e.getMessage());
		} finally {// closing the costly resources.
			file.close();
			scanner.close();
		}
	}

}
