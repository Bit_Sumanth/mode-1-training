package com.hcl.assignment07;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a java program to read the input character stream and identify 
 *         patterns provided by the user. As the program output display the
 *         number of times the pattern occurred in the input character stream.
 */

public class Exercise07 {

	// reading a file, private for encapsulation.
	private String readAFile() throws IOException {

		java.io.FileReader reader = null;
		BufferedReader bufferedReader = null;
		String text = "";
		try {

			String basePath = new File("").getAbsolutePath();
			String relative = basePath.concat("//src//com//hcl//assignment07//ReadATextFile.txt");
			reader = new FileReader(relative);
			bufferedReader = new BufferedReader(reader);
			String line;
			try {
				// while loop to read the text untill it becomes null value.
				while ((line = bufferedReader.readLine()) != null) {

					// System.out.println(line);
					text += line;

				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}

		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		} finally {

			// Here Both Lines Throws IOException which are finally handled by JVM.
			// closing the costly resources gracefully.

			reader.close();
			bufferedReader.close();
		}
		return text;

	}

	// business logic.
	public String searchAPatternAndCountOccurence(String[] strings) throws IOException {
		String string = readAFile();

		String result = "";
		System.out.println("Given string is : " + string);
		int count = 0;
		for (int i = 0; i < strings.length; i++) {
			if (string.contains(strings[i])) {
				++count;
			}
			result += "Word : " + strings[i] + " Count : " + count + "\n";

		}
		return result;

	}
}
