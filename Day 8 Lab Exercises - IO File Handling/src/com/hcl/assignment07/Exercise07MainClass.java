package com.hcl.assignment07;

import java.io.IOException;
import java.util.Scanner;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a java program to read the input character stream and identify 
 *         patterns provided by the user. As the program output display the
 *         number of times the pattern occurred in the input character stream.
 */
public class Exercise07MainClass {

	public static void main(String[] args) {

		// prompt the user.
		System.out.println("Enter the Number of Words.");

		Scanner scanner = new Scanner(System.in);
		int noOfWords = scanner.nextInt();

		System.out.println("Enter the Strings to be searched.");
		String[] strings = new String[noOfWords];
		for (int i = 0; i < noOfWords; i++) {

			strings[i] = scanner.next();
		}

		// closing the costly resource.
		scanner.close();

		// object instantiation.
		Exercise07 exercise07 = new Exercise07();

		try {
			String ans = exercise07.searchAPatternAndCountOccurence(strings);
			System.out.println(ans);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

		// de-referencing
		exercise07 = null;
	}

}
