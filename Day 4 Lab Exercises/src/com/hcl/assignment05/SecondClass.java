package com.hcl.assignment05;

public class SecondClass {

	// instance variable == refered using instance of a class.
	double b = 123.45;

	// arg-constructor.
	public SecondClass() {
		System.out.println("-----in the constructor of class B: ");
		System.out.println("b = " + b);
		b = 3.14159;
		System.out.println("b = " + b);
	}

	// getters and setters.
	public void setSecondClass(double value) {
		b = value;
	}

	public double getSecondClass() {
		return b;
	}

}
