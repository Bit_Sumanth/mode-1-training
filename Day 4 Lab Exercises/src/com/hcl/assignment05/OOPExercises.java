package com.hcl.assignment05;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Output of the following program.?
 *
 */
public class OOPExercises {

	public static void main(String[] args) {

		// instantiating the objects of class (Eager loading)
		FirstClass objA = new FirstClass();
		SecondClass objB = new SecondClass();

		System.out.println("in main(): ");

		// initializing the fields by respective object reference.
		System.out.println("objA.a = " + objA.getFirstClass());

		System.out.println("objB.b = " + objB.getSecondClass());

		objA.setFirstClass(222);

		objB.setSecondClass(333.33);

		System.out.println("objA.a = " + objA.getFirstClass());

		System.out.println("objB.b = " + objB.getSecondClass());

		// output of program.
		/*
		 * in the constructor of class FirstClass: a = 100 a = 333 -----in the
		 * constructor of class B: b = 123.45 b = 3.14159 in main(): objA.a = 333 objB.b
		 * = 3.14159 objA.a = 222 objB.b = 333.33
		 * 
		 * 
		 */

	}

}
