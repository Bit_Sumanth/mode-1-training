package com.hcl.assignment05;

public class FirstClass {

	// non-static varible.
	int a = 100;

	// arg-constructor
	public FirstClass() {
		System.out.println("in the constructor of class FirstClass: ");
		System.out.println("a = " + a);
		a = 333; // re-intializing the vaule.
		System.out.println("a = " + a);
	}

	// getters and setters.
	public void setFirstClass(int value) {
		a = value;
	}

	public int getFirstClass() {
		return a;
	}

}
