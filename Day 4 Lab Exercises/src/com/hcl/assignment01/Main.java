package com.hcl.assignment01;

import java.util.Scanner;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         create instances of the above classes and test the above classes.
 */
public class Main {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Circle");
		System.out.println("Square");
		System.out.println("Rectagle");

		System.out.println("Enter the Shape Name : ");

		String choice = scanner.next();

		switch (choice) {

		case "Circle": {
			Shape circleShape = new Circle();
			int input = scanner.nextInt();
			((Circle) circleShape).setRadius(input);
			System.out.println("Area of the Circle is : " + circleShape.calculateArea());
			break;
		}

		case "Square": {
			Shape sqaureShape = new Square();
			int input = scanner.nextInt();
			((Square) sqaureShape).setSide(input);
			System.out.println("Area of the Sqare is : " + sqaureShape.calculateArea());
			break;
		}

		case "Rectangle": {
			Shape rectangle = new Rectangle();
			int length = scanner.nextInt();
			int breadth = scanner.nextInt();
			((Rectangle) rectangle).setLength(length);
			((Rectangle) rectangle).setBreadth(breadth);
			System.out.println("Area of Rectangle is : " + rectangle.calculateArea());
			break;
		}

		default:
			System.out.println("Invalid Choice.");

		}

		// closing the costly resources
		scanner.close();
	}

}
