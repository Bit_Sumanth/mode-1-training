package com.hcl.assignment01;

public class Rectangle extends Shape {

	// fields
	private int length;
	private int breadth;

	public Rectangle() {
		super();
		System.out.println("Enter the length :");
		System.out.println("Enter the breadth :");
	}

	// arg-constructor
	public Rectangle(String name, int length, int breadth) {
		super(name);
		this.length = length;
		this.breadth = breadth;

	}

	// getters and setters
	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getBreadth() {
		return breadth;
	}

	public void setBreadth(int breadth) {
		this.breadth = breadth;
	}
	
	@Override
	float calculateArea() {
		return length * breadth;
	}

	

	
}
