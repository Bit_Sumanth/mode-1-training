package com.hcl.assignment01;

/**
 * 
 * @author Sumanth Babu.
 *
 */
public abstract class Shape {
	protected String name;

	public Shape() {
		super();

	}

	// arg-constructor
	public Shape(String name) {
		this.name = name;
	}

	// getters and setters

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// abstract method
	abstract float calculateArea();

}
