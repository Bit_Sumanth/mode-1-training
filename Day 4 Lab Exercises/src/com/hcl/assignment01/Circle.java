package com.hcl.assignment01;

public class Circle extends Shape {

	// fields
	private int radius;
	private final float PI = 3.14F;

	public Circle() {
		super();
		System.out.println("Enter the radius : ");
	}

	// arg-constructor
	public Circle(String name, int radius) {
		super(name);
		this.radius = radius;
	}

	// getters and setters
	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	@Override
	float calculateArea() {
		return PI * this.radius * this.radius;
	}

}
