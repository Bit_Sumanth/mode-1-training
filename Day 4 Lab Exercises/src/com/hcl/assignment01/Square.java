package com.hcl.assignment01;

public class Square extends Shape {

	// fields
	private int side;

	public Square() {
		super();
		System.out.println("Enter the side dimension : ");
	}

	// arg-constructor
	public Square(String name, int side) {
		super(name);
		this.side = side;
	}

	// getters and setters
	public int getSide() {
		return side;
	}

	public void setSide(int side) {
		this.side = side;
	}
	
	@Override
	float calculateArea() {
		return this.side * this.side;
	}

	

}
