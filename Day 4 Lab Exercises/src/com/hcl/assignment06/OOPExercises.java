package com.hcl.assignment06;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Output of the following program.?
 */
public class OOPExercises {

	public static void main(String[] args) {

		A objA = new A();

		B objB1 = new B();

		A objB2 = new B();

		C objC1 = new C();

		// B objC2 = new C(); gives compile time error.

		A objC3 = new C();

		objA.display();

		objB1.display();

		objB2.display();

		objC1.display();

		// objC2.display();

		objC3.display();

		// output of the program.
		/*
		 * a in A = 100 
		 * a in B = 123 
		 * a in B = 123 
		 * a in C = 543 
		 * a in C = 543
		 * 
		 */
	}

}
