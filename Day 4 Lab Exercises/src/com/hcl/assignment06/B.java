package com.hcl.assignment06;

//inheritence
public class B extends A {

	private int a = 123;

	// ovveriding the parent class functionality.
	public void display() {
		System.out.printf("a in B = %d\n", a);
	}
}
