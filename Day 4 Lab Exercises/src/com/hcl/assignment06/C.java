package com.hcl.assignment06;

// inheritence
public class C extends A {

	private int a = 543;

	// ovveriding the parent class functionality.
	public void display() {
		System.out.printf("a in C = %d\n", a);
	}
}
