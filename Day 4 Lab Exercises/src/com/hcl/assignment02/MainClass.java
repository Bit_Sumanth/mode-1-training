package com.hcl.assignment02;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         To find the sum of divisors of a number.
 */
public class MainClass {

	public static void main(String[] args) {

		// instance is created
		MyCalculator myCalculator = new MyCalculator();

		System.out.print("I implemented: AdvancedArithmetic \n");

		int ans = myCalculator.divisor_sum(6); // static data passing.

		System.out.println(ans);
	}

}
