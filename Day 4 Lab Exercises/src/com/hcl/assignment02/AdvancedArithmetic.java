package com.hcl.assignment02;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         To find the sum of divisors of a number.
 */
public interface AdvancedArithmetic {
	int divisor_sum(int num); // (By default) Abstract Method
}
