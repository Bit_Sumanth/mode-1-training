package com.hcl.assignment04;

/**
 * 
 * @author Sumanth Babu.
 * 
 * 
 *         Explain the errors of the code.
 *
 */
public class OOPExercises {

	@SuppressWarnings("unused")
	public static void main(String[] args) {

		// instantiated.
		A objA = new A();

		System.out.println("in main(): ");

		/*
		 * System.out.println("objA.a = "+objA.a); objA.a = 222;
		 * 
		 * For above two code lines. Compile Time Error is
		 * "The field A.a is not visible"
		 * 
		 * Reason: The access specifier for 'a' is private.
		 * 
		 * For private access specifier members the visibility of members is with in
		 * that class only.
		 */

		// de-referencing
		objA = null;
	}

}
