package com.hcl.assignment04;

public class A {

	// instance variable (Encapsulated) == security
	private int a = 100;

	// setters and getters.
	public void setA(int value) {
		a = value;
	}

	public int getA() {
		return a;
	}
}
