package com.hcl.assignment03;

public class PaybackCard extends Card {

	// instance fileds
	private int pointsEarned;
	private double totalAmount;

	// arg-constructor
	public PaybackCard(String holderName, String cardNumber, String expiryDate, int pointsEarned, double totalAmount) {
		super(holderName, cardNumber, expiryDate);
		this.cardNumber = cardNumber;
		this.expiryDate = expiryDate;
	}

	// getters and setters
	public int getPointsEarned() {
		return pointsEarned;
	}

	public void setPointsEarned(int pointsEarned) {
		this.pointsEarned = pointsEarned;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

}
