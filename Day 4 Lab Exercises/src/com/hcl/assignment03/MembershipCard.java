package com.hcl.assignment03;

public class MembershipCard extends Card {

	// non static data member.
	private int rating;

	// arg-constructor
	public MembershipCard(String holderName, String cardNumber, String expiryDate, int rating) {
		super(holderName, cardNumber, expiryDate);
		this.rating = rating;
	}

	// getters and setters
	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

}
