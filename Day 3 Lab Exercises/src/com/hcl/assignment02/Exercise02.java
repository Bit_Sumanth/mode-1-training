package com.hcl.assignment02;

/**
 * 
 * @author Sumanth Babu.
 *
 */
public class Exercise02 {

	public String replaceDToH(String string) {

		char[] stringArray = string.toCharArray();

		for (int i = 0; i < stringArray.length; i++) {
			if (stringArray[i] == 'd')
				stringArray[i] = 'h';
		}
		return String.valueOf(stringArray);
	}
}
