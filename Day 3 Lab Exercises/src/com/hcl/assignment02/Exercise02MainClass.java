package com.hcl.assignment02;

/**
 * 
 * @author Sumanth Babu.
 *
 *
 *         To replace all the 'd' occurrence characters with �h� characters
 */
public class Exercise02MainClass {

	public static void main(String[] args) {

		// instantating the Excersice class.
		Exercise02 exercise02 = new Exercise02();

		String string = exercise02.replaceDToH("daddy");

		System.out.println(string);// displaying output

		// de-referencing
		exercise02 = null;
	}

}
