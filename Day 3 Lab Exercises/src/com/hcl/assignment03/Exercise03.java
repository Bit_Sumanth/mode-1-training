package com.hcl.assignment03;

public class Exercise03 {

	public int[] sortIntArrayInAscending(int[] array) {

		int temp = 0;

		// Sort the array in ascending order
		for (int i = 0; i < array.length; i++) {
			for (int j = i + 1; j < array.length; j++) {
				if (array[i] > array[j]) {
					temp = array[i];
					array[i] = array[j];
					array[j] = temp;
				}
			}
		}

		return array;

	}
}
