package com.hcl.assignment03;

import java.util.Arrays;

/**
 * 
 * @author Sumanth Babu.
 *		
 *			To sort an integer array of 10 elements in ascending. 
 */
public class Exercise03MainClass {

	public static void main(String[] args) {
		// unsorted array.
		// way 1 (Using Inbuild method)
		/*
		 * int[] intArray = { 10, 2, 9, 4, 5, 3, 7, 8, 6, 1 };
		 * 
		 * Arrays.sort(intArray);
		 * 
		 * for (int element : intArray) { System.out.print(element + " "); }
		 */

		// way 2 custom
		int[] intArray = { 10, 2, 9, 4, 5, 3, 7, 8, 6, 1 };
		Exercise03 exercise03 = new Exercise03();
		int[] ans = exercise03.sortIntArrayInAscending(intArray);
		System.out.println("The sorted array is : " + Arrays.toString(ans));

		// de-referencing

		exercise03 = null;
	}

}
