package com.hcl.assignment06;

/**
 * 
 * @author Sumanth Babu.
 *
 */
public class Exercise06 {

	// logic
	public String isPalindrome(String str) {
		StringBuilder sb = new StringBuilder(str);
		sb.reverse();
		String rev = sb.toString();
		if (str.equals(rev)) {
			return "Yes it is a Palindrome.";
		} else {
			return "No it's not a Palindrome.";
		}

	}
}
