package com.hcl.assignment06;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         String Palindrome?
 */
public class Exercise06MainClass {
	public static void main(String[] args) {

		// instantiating
		Exercise06 exercise06 = new Exercise06();

		String ans = exercise06.isPalindrome("madam");

		System.out.println(ans);

		// de-referencing
		exercise06 = null;
	}
}
