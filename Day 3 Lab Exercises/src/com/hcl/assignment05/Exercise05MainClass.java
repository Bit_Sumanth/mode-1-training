package com.hcl.assignment05;

/**
 * 
 * @author Sumanth Babu.
 *
 *         print the substring consisting of all characters inclusive range from
 *         ..to .
 */
public class Exercise05MainClass {

	public static void main(String[] args) {

		// Static Data
		String string = "Helloworld";
		System.out.println(string.substring(3, 7));
	}

}
