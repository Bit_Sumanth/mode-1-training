package com.hcl.assignment08;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         processing the given input string acording to rules.
 * 
 *         Keep the first char if it is 'k'
 * 
 *         Keep the second char if it is 'b'.
 */
public class UserMainCode {

	public static void main(String[] args) {

		// static data.
		String ans = getString("hello");

		System.out.println(ans);
	}

	// partial logic implementaion.
	public static String getString(String string) {
		if (string.charAt(0) == 'k' && string.charAt(1) == 'b')
			return string;
		else
			return string.substring(2);

	}
}
