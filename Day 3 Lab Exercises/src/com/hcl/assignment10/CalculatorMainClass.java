package com.hcl.assignment10;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         How to use add method from another package?
 */
public class CalculatorMainClass {

	public static void main(String[] args) {

		/*
		 * Import the required package, instantate object of that class and call the
		 * method.
		 */

		Calculator calculator = new Calculator();
		int ans = calculator.add(5, 3);
		System.out.println("The Addition of Two Numbers is : " + ans);

		// de-referencing
		calculator = null;
	}

}
