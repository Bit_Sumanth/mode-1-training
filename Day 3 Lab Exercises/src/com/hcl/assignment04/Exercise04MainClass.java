package com.hcl.assignment04;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         To search for an element of an integer array of 10 elements.?
 */
public class Exercise04MainClass {

	public static void main(String[] args) {

		// int[] intArray = { 10, 2, 9, 4, 5, 3, 7, 8, 6, 1 };
		// way 1
		// works only for sorted array. (build In method)

		// integer data type array declaration and initialisation.
		int[] intArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		/*
		 * int ans = Arrays.binarySearch(intArray, 3);
		 * System.out.println("The Position of the element is " + ans);
		 */

		// way 2
		Exercise04 exercise04 = new Exercise04();
		if (exercise04.searchAnElementInArray(intArray, 00) != -1)
			System.out.println("The element is found");
		else
			System.out.println("The element is not found");

		// de-referncing
		exercise04 = null;
	}

}
