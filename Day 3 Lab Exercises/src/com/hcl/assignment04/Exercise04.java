package com.hcl.assignment04;

/**
 * 
 * @author Sumanth Babu.
 *
 */
public class Exercise04 {

	// Logic
	public int searchAnElementInArray(int[] array, int element) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == element)
				return element;
		}
		return -1;
	}
}
