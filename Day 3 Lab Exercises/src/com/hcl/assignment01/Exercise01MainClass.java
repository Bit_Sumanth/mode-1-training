package com.hcl.assignment01;

/**
 * 
 * @author Sumanth babu.
 * 
 *         To convert all the characters in a string to lowercase.
 */
public class Exercise01MainClass {

	public static void main(String[] args) {

		String string = "HCL TECHNOLOGIES";
		
		string = string.toLowerCase();	//build in method in String class.
		
		System.out.println(string);
	}

}
