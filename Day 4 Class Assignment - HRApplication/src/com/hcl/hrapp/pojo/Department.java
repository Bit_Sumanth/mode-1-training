package com.hcl.hrapp.pojo;

/**
 * 
 * @author Sumanth Babu.
 *
 */
// POJO Class
public class Department {

	// departmet == dept (shorthand)
	private int deptNo;
	private String deptName;
	private Employee employee;

	public Department() { // no-arg constructor.
		super();
	}

	public Department(int deptNo, String deptName, Employee employee) {
		this.deptNo = deptNo;
		this.deptName = deptName;
		this.employee = employee;
	}

	public int getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(int deptNo) {
		this.deptNo = deptNo;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

}
