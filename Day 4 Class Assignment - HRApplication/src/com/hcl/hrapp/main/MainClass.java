package com.hcl.hrapp.main;

import com.hcl.hrapp.pojo.Department;
import com.hcl.hrapp.pojo.Employee;
import com.hcl.hrapp.service.HrService;

/**
 * 
 * @author Sumanth Babu.
 *
 */
public class MainClass {

	public static void main(String[] args) {

		Department department1 = new Department();
		department1.setDeptNo(0101);
		department1.setDeptName("Training");
		Employee employee1 = new Employee();
		employee1.setEmpId(100);
		employee1.setEmpName("Krishna");
		department1.setEmployee(employee1);

		Employee employee2 = new Employee(200, "Damodara");
		Department department2 = new Department(0102, "Development", employee2);

		Employee employee3 = new Employee(300, "Vasudeva");
		Department department3 = new Department(0103, "Training", employee3);

		Employee employee4 = new Employee(400, "Damodara");
		Department department4 = new Department(0104, "Development", employee4);

		Department[] departments = new Department[4];
		departments[0] = department1;
		departments[1] = department2;
		departments[2] = department3;
		departments[3] = department4;

		HrService hrService = new HrService();
		String ans = hrService.searchByEmployeeName(departments, "Vasudeva");
		System.out.println(ans);

		// De- referencing
		department1 = null;
		department2 = null;
		department3 = null;
		department4 = null;

	}

}
