package com.hcl.hrapp.service;

import com.hcl.hrapp.pojo.Department;

/**
 * 
 * @author Sumanth Babu.
 *
 */
public class HrService {

	// partially working to the business logic
	public String searchByEmployeeName(Department[] departments, String name) {
		int noOfEmployees = 0;
		if (departments.length != 0 && name != "") {
			String flag = "";
			for (Department department : departments) {

				if (department.getEmployee().getEmpName().equalsIgnoreCase(name)) {
					++noOfEmployees;
					flag = " ";
				} else {
					flag = "not";
				}
			}

			return name + " is " + flag + " present " + " " + noOfEmployees + " times ";
		} else {
			return "wrong details";
		}
	}
}
