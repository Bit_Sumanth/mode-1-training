package com.hcl.assignment10;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a program to read two String variables in DD-MM-YYYY.Compare
 *         the two dates and return the older date in 'MM/DD/YYYY' format.?
 */
public class UserMainCode {

	public static void main(String[] args) throws ParseException {

		// Scanner Object creation.
		Scanner scanner = new Scanner(System.in);

		String stringDate1 = scanner.nextLine();// reading input data form console.
		String stringDate2 = scanner.nextLine();

		String ans = findOldDate(stringDate1, stringDate2);// method calling.

		// displaying output.
		System.out.println("Older Date is : " + ans);

		// closing the costly resource.
		scanner.close();

	}

	// Business Logic.
	public static String findOldDate(String stringDate1, String stringDate2) throws ParseException {

		// creating SimpleDateFormat objects from import java.text.SimpleDateFormat;
		SimpleDateFormat simpleDateFormate1 = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat simpleDateFormate2 = new SimpleDateFormat("MM/dd/yyyy");

		// Date objects fromed by parsing the input string dates from user.
		Date date1 = simpleDateFormate1.parse(stringDate1);
		Date date2 = simpleDateFormate1.parse(stringDate2);

		// Calender instance.
		Calendar calender = Calendar.getInstance();
		calender.setTime(date1);
		long time1 = calender.getTimeInMillis();
		calender.setTime(date2);

		long time2 = calender.getTimeInMillis();

		String s3 = simpleDateFormate2.format(date1);
		String s4 = simpleDateFormate2.format(date2);
		if (time1 < time2)// comparing the time values from two date objects.
			return s3;
		else
			return s4;// returning the older objects with if statement logic.

	}
}
