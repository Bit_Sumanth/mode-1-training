package com.hcl.assignment14;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Given a date string in the format dd/mm/yyyy, write a program to
 *         convert the given date to the format dd-mm-yy.?
 */
public class UserMainCode {

	public static void main(String[] args) {

		Date date = new Date();// Date Object Creation.

		// DateFormatter Object Creation.
		SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy");

		String stringDate = DateFor.format(date);// formatting the date

		System.out.println("Date Format with dd/MM/yyyy : " + stringDate);

		System.out.println("Date Format with dd-MM-yyyy : " + convertDateFormat(stringDate));

		// de-referencing
		date = null;
	}

	// Business Logic.
	public static String convertDateFormat(String stringDate) {

		Date date = new Date();// Date object creation

		SimpleDateFormat DateFor = new SimpleDateFormat("dd-MM-yyyy");

		stringDate = DateFor.format(date);

		return stringDate;// returning the formated string value.

	}
}
