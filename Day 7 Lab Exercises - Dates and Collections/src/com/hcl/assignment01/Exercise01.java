package com.hcl.assignment01;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         To print current date and time in the specified format.
 */
public class Exercise01 {

	public static void main(String[] args) {

		/* by using old legacy class ie., (java.util.Date) */

		Date date = new Date(); // Date object is created.
		System.out.println("Displaying Date and Time using legacy Class : " + date);// printing the date to console.

		/* by using Java 8 featured, (java.time.LocalDateTime class) */

		// LocalDateTime Object Creation

		/*
		 * LocalDateTime.now() this static method returns: the current date-time using
		 * the system clock.
		 */
		LocalDateTime currentDateandTime = LocalDateTime.now();

		/*
		 * Formatting the Date Object, by using java.time.format.DateTimeFormatter class
		 * with specified pattern.
		 */

		/*
		 * Formatting styles: 
		 * 1) yyyy-MM-dd -> "1988-09-29" 
		 * 2) dd/MM/yyyy -> "29/09/1988" 
		 * 3) dd-MMM-yyyy -> "29-Sep-1988" 
		 * 4) E, MMM dd yyyy -> "Thu, Sep 29 1988"
		 */
		DateTimeFormatter dateTimeformatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

		System.out.println(
				"Displaying Date and Time using Date API Class :  " + dateTimeformatter.format(currentDateandTime));

		// de-referncing the objects.
		date = null;
		currentDateandTime = null;
		dateTimeformatter = null;

	}

}
