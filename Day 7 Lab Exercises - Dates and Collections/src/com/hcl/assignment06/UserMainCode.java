package com.hcl.assignment06;

import java.util.Calendar;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Create a class Main which would get 2 integers as input and call the
 *         static method getNumberOfDays present in the UserMainCode. The method
 *         returns an integer corresponding to the number of days in the month.?
 */
public class UserMainCode {

	public static void main(String[] args) {

		// method calling and displaying the results with a label.
		System.out.println("No of Days in Month : " + getNumberOfDays(2000, 1));
	}

	// business logic with static modifier as guided in workbook.
	public static int getNumberOfDays(int year, int month) {
		// calender instance.
		Calendar calendar = Calendar.getInstance();

		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.YEAR, year);

		int dayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

		return dayOfMonth;// returning the result.

	}

}
