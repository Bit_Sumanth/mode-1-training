package com.hcl.assignment02;

import java.time.LocalDateTime;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a Java program to extract date, time from the date string.
 */
public class Exercise02 {

	public static void main(String[] args) {

		// The static method returns, LocalDateTime Object.
		LocalDateTime currentDateandTime = LocalDateTime.now();

		// Two possible ways to convert Object to String, in Java.
		String dateAndTimeString = currentDateandTime.toString();
		// String dateAndTimeString = String.valueOf(currentDateandTime);

		// Array of String values.
		String[] strings = dateAndTimeString.split("T");

		// Assigning to respective variables for displaying output.
		String dateString = strings[0];
		String timeString = strings[1];

		System.out.println("Date String : " + dateString);

		System.out.println("Time String : " + timeString);

		// de-referincing the necessary objects.
		currentDateandTime = null;
		strings = null;
	}

}
