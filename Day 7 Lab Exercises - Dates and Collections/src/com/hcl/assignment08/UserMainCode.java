package com.hcl.assignment08;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 
 * @author Sumanth Babu.
 * 
 *          Include a class UserMainCode with a static
 *         method sortMergedArrayList which accepts 2 ArrayLists.
 * 
 *          The return type is an ArrayList with elements from 2,6 and 8th index
 *         position .Array index starts from position 0.
 */
public class UserMainCode {

	public static void main(String[] args) {

		// building the two array list manually (static data)
		ArrayList<Integer> arrayList1 = new ArrayList<Integer>(5);// initial capacity of ArrayList 5
		arrayList1.add(3);
		arrayList1.add(1);
		arrayList1.add(17);
		arrayList1.add(11);
		arrayList1.add(19);
		ArrayList<Integer> arrayList2 = new ArrayList<Integer>(5);
		arrayList2.add(5);
		arrayList2.add(2);
		arrayList2.add(7);
		arrayList2.add(6);
		arrayList2.add(20);

		ArrayList<Integer> result = sortMergedArrayList(arrayList1, arrayList2);

		result.stream().forEach((element) -> System.out.println(element));

		// de-referencing
		arrayList1 = null;
		arrayList2 = null;
	}

	// business logic
	public static ArrayList<Integer> sortMergedArrayList(ArrayList<Integer> list1, ArrayList<Integer> list2) {

		// Downcasting to ArrayList Type. Stream pipelining
		ArrayList<Integer> list = (ArrayList<Integer>) Stream.concat(list1.stream(), list2.stream())
				.collect(Collectors.toList());

		Collections.sort(list);// Natural Ordering

		// Upcasting == Generalization.
		List<Integer> result = new ArrayList<Integer>();
		result.add(list.get(2));
		result.add(list.get(6));
		result.add(list.get(8));

		return (ArrayList<Integer>) result;// returning the result.

	}
}
