package com.hcl.assignment04;

import java.util.Calendar;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a Java program to get the maximum value of the year, month,
 *         week, date from the current date of a default calendar.?
 */
public class Exercise04 {

	public static void main(String[] args) {

		// Calender object creation.
		Calendar calendar = Calendar.getInstance();

		System.out.println("Current Date and Time : " + calendar.getTime());

		// Maximum == Max
		System.out.println("Max value of the Year : " + calendar.getActualMaximum(Calendar.YEAR));

		System.out.println("Max value of the Month : " + calendar.getActualMaximum(Calendar.MONTH));

		System.out.println("Max value of the Week : " + calendar.getActualMaximum(Calendar.WEEK_OF_YEAR));

		System.out.println("Max value of the Date : " + calendar.getActualMaximum(Calendar.DATE));

		// de-referencing the used object.
		calendar = null;

	}

}
