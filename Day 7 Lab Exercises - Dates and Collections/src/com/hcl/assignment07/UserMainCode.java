package com.hcl.assignment07;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         To Test whether first and last characters are same in given string.
 */
public class UserMainCode {

	public static void main(String[] args) {

		// calling the business logic with passing parameter.
		int ans = checkCharacters("the picture was great");

		if (ans == 1) {// condition check.
			System.out.println("Valid");
		} else {
			System.out.println("Invalid");
		}
	}

	// declared method as static, as it was mentioned in workbook.
	public static int checkCharacters(String string) {

		// converting all characters to lower case.
		String lowerString = string.toLowerCase();
		char[] charArray = lowerString.toCharArray();

		// comparing first index character and last index character.
		if (charArray[0] == charArray[string.length() - 1])
			return 1;

		return 0;// returning the value.

	}

}
