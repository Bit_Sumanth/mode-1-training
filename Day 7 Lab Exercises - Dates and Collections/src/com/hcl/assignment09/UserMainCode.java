package com.hcl.assignment09;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * 
 * @author Sumanth Babu.
 *
 */
public class UserMainCode {

	public static void main(String[] args) {

		// Interface var = new InterfaceImpl(); == Abstraction.
		Map<String, String> employeeDetails = new HashMap<>();

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter, How many number of details to be added ?");

		int noOfDetails = 0;
		try {
			noOfDetails = scanner.nextInt();
		} catch (InputMismatchException e) {
			System.out.println("Enter only Numbers");

		}

		if (noOfDetails <= 0)
			System.out.println("Enter a Valid Number.");

		// Building the HashMap Dynamically from command propmt (User Data).
		for (int i = 0; i < noOfDetails; i++) {

			System.out.println("Enter Employee Name : ");

			@SuppressWarnings("resource")
			// using scanner object with a space as delimiter, to avoid unnecessary inputs.
			Scanner empNameScanner = new Scanner(System.in).useDelimiter("\\s");
			String employeeName = empNameScanner.next();

			UserMainCode.emptyStringCheck(employeeName);

			System.out.println("Enter " + employeeName + " Designation : ");

			@SuppressWarnings("resource")
			Scanner designationScanner = new Scanner(System.in).useDelimiter("\\s");
			String designation = designationScanner.next();

			UserMainCode.emptyStringCheck(designation);

			/*
			 * Duplicate Keys are not allowed in HashMap, but Duplicate Values are allowed.
			 */

			employeeDetails.put(employeeName, designation);// adding key-value pair to HashMap collection object.

		}

		scanner.close();// closing the costly resource.

		String[] result = obtainDesignation(employeeDetails, "CEO");// method calling.
		// iterating and displaying results.
		for (String string : result) {
			System.out.println(string);
		}
	}

	// Business Logic.
	public static String[] obtainDesignation(Map<String, String> details, String designation) {

		Map<String, String> finalMap = details.entrySet().stream().filter((e) -> e.getValue().equals(designation))
				.collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));

		List<String> list = new ArrayList<String>(finalMap.keySet());

		String[] result = list.toArray(new String[list.size()]);

		return result;

	}

	// Valiadation Logic.
	public static void emptyStringCheck(String stringValue) {
		if (stringValue == null || stringValue.length() <= 0) {
			System.out.println("Enter Valid Employee Details ");
			System.exit(0);
		}
	}
}
