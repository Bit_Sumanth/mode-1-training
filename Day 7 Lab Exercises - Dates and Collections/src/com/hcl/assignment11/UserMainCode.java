package com.hcl.assignment11;

import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Given a method with two date strings in yyyy-mm-dd format as input.
 *         Write code to find the difference between two dates in months?
 */
public class UserMainCode {

	public static void main(String[] args) {

		// Scanner object creation.
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter Date 1 : ");
		String localDate1 = scanner.next();

		System.out.println("Enter Date 2 : ");
		String localDate2 = scanner.next();

		scanner.close();// closing the scanner object.

		// local objects creation from parse method.
		LocalDate localDate1Obj = LocalDate.parse(localDate1);

		LocalDate localDate2Obj = LocalDate.parse(localDate2);

		String date1 = localDate1Obj.toString();

		String date2 = localDate2Obj.toString();

		// method calling and displaying the results.
		System.out.println("Difference between two months : " + getMonthDifference(date1, date2));

	}

	// business logic
	public static int getMonthDifference(String date1, String date2) {
		LocalDate localDate1 = LocalDate.parse(date1);
		LocalDate localDate2 = LocalDate.parse(date2);
		Period period = Period.between(localDate1, localDate2);
		return period.getMonths();// returning the months difference value.
	}
}
