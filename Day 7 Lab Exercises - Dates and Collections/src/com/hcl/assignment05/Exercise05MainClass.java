package com.hcl.assignment05;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Given two inputs year and month (Month is coded as: Jan=0, Feb=1
 *         ,Mar=2 ...), write a program to find out total number of days in the
 *         given month for the given year.?
 */
public class Exercise05MainClass {

	public static void main(String[] args) {

		// instantiation
		Exercise05 exercise05 = new Exercise05();

		// method calling and printing 					//march is coded as 03 and year
		System.out.println("No of Days in Month " + exercise05.noOfDaysInAMonth(03, 2021));

		// de-referencing
		exercise05 = null;

	}

}
