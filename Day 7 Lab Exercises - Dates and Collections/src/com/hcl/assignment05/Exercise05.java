package com.hcl.assignment05;

import java.util.Calendar;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Given two inputs year and month (Month is coded as: Jan=0, Feb=1
 *         ,Mar=2 ...), write a program to find out total number of days in the
 *         given month for the given year.?
 */
public class Exercise05 {

	// logic
	public int noOfDaysInAMonth(int month, int year) {

		Calendar calendar = Calendar.getInstance();// calender object creation.

		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.YEAR, year);// setting the input parameter values.

		int dayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

		return dayOfMonth;// retuning no.of.days.

	}
}
