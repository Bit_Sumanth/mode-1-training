package com.hcl.assignment03;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a Java program to get and display information (year, month,
 *         day, hour, minute) of a default calendar.?
 */
public class Exercise03 {

	public static void main(String[] args) {

		// Object creation
		LocalDateTime currentDateandTime = LocalDateTime.now();

		// static method returns, DateTimeFormatter object.
		DateTimeFormatter dateTimeformatter = DateTimeFormatter.ofPattern("yyyy/MM/E HH:mm");

		// formating the current date and time object with formater object and
		// displaying result.
		System.out.println("Current Date and Time :  " + dateTimeformatter.format(currentDateandTime));

		// de-referencing
		currentDateandTime = null;
		dateTimeformatter = null;

	}

}
