package com.hcl.assignment13;

import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Get two date strings as input and write code to find difference
 *         between two dates in days.?
 */
public class UserMainCode {

	public static void main(String[] args) {

		// Scanner Object Creation.
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter Date 1 : ");
		String localDate1 = scanner.next();// reading inputs at run time.

		System.out.println("Enter Date 2 : ");
		String localDate2 = scanner.next();

		scanner.close();// closing the costly resources.

		// LocalDate objects creation.
		LocalDate localDate1Obj = LocalDate.parse(localDate1);

		LocalDate localDate2Obj = LocalDate.parse(localDate2);

		// converting LocalDate objects to string objects.
		String date1 = localDate1Obj.toString();
		String date2 = localDate2Obj.toString();

		// calling the business logic and displaying results.
		System.out.println("Difference between two dates : " + getDateDifference(date1, date2));
	}

	// Business Logic.
	public static int getDateDifference(String date1, String date2) {

		LocalDate localDate1 = LocalDate.parse(date1);
		LocalDate localDate2 = LocalDate.parse(date2);
		Period period = Period.between(localDate1, localDate2);

		return period.getDays(); // retuning the data difference.

	}
}
