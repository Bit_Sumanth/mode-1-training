package com.hcl.assignment12;

import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a program to read a string and validate the IP address. Print
 *         �Valid� if the IP address is valid, else print �Invalid�.?
 */
public class UserMainCode {

	public static void main(String[] args) {

		// Scanner Object Instantiation.
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the IP Address.");

		String ipAddress = scanner.nextLine();// reading value at runtime.

		scanner.close();// closing the scanner object.

		int result = ipValidator(ipAddress);// method call.

		if (result == 1) {// validating based on the output of method call.
			System.out.println("Valid");
		} else {
			System.out.println("Invalid");
		}
	}

	// Business Logic
	public static int ipValidator(String ipAddress) {

		// String Tokenizer object creation.
		StringTokenizer stringTokenizer = new StringTokenizer(ipAddress, ".");// to break a string into tokens.

		// parsing the values and storing in local variables.
		int a = Integer.parseInt(stringTokenizer.nextToken());
		int b = Integer.parseInt(stringTokenizer.nextToken());
		int c = Integer.parseInt(stringTokenizer.nextToken());
		int d = Integer.parseInt(stringTokenizer.nextToken());

		// comparision
		if ((a >= 0 && a <= 255) && (b >= 0 && b <= 255) && (c >= 0 && c <= 255) && (d >= 0 && d <= 255))
			return 1;
		else
			return -1;

	}
}
