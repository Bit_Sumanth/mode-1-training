package com.hcl.assignment05;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         To find a given number is prime or not?
 */
public class Exercise05MainClass {

	public static void main(String[] args) {

		// object creation
		Exercise05 exercise05 = new Exercise05();

		// passing static data as argument to function.
		exercise05.checkPrime(10);

		// de-referencing
		exercise05 = null;
	}

}
