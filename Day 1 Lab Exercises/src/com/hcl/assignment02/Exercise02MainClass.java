package com.hcl.assignment02;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         write methods for addition, subraction, multiplicatoion, division,
 *         remainder of two numbers as input..?
 */
public class Exercise02MainClass {

	public static void main(String[] args) {

		// Given static data is
		// num1 = 20
		// num2 = 4

		Exercise02 exercise02 = new Exercise02();// object creation.

		// calling the business logics by ObjectReference.
		exercise02.additionOfTwoNumbers(20, 4);
		exercise02.subtractionOfTwoNumbers(20, 4);
		exercise02.multiplicationOfTwoNumbers(20, 4);
		exercise02.divisionOfTwoNumbers(20, 4);
		exercise02.remainderOfTwoNumbers(20, 40);

		// de-referencing
		exercise02 = null;
	}

}
