package com.hcl.assignment02;

public class Exercise02 {

	// addition
	public int additionOfTwoNumbers(int num1, int num2) {
		return num1+num2;
	}

	// subtraction
	public int subtractionOfTwoNumbers(int num1, int num2) {
		return num1-num2;
	}

	// multiplication
	public int multiplicationOfTwoNumbers(int num1, int num2) {
		return num1*num2;
	}

	// division
	public int divisionOfTwoNumbers(int num1, int num2) {
		int ans=0;
		try {
			ans= num1 / num2;	
		} catch (ArithmeticException e) { //Handling the / by zero exception case.
			System.err.println("Solution");
		}
		return ans;
	}

	// remainder
	public int remainderOfTwoNumbers(int num1, int num2) {
		return num1%num2;
	}
}
