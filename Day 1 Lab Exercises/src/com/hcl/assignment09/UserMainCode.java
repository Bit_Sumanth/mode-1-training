package com.hcl.assignment09;

import java.util.Scanner;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         To find the sum of squares of even digits.?
 */
public class UserMainCode {

	public static void main(String[] args) {

		// Scanner object creation.
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter a positive number.");

		// reading the input from user. (Dynamic data)
		int num = scanner.nextInt();

		int ans = sumOfSquaresOfEvenDigits(num);

		System.out.println("The sum of squares of even digits are : " + ans);
		// closing the costly resources gracefully..
		scanner.close();
	}

	public static int sumOfSquaresOfEvenDigits(int num) {
		int sum = 0;
		while (num > 0) {
			int rem = num % 10;
			if (rem % 2 == 0)
				sum += (rem * rem);
			num /= 10;
		}
		return sum;
	}
}
