package com.hcl.assignment07;

/**
 * 
 * @author Sumanth Babu
 * 
 *         For multiples of three print "Fizz" instead of the number and print
 *         "Buzz" for the multiples of five. When number is divided by both
 *         three and five, print "fizz buzz".
 */
public class Exercise07MainClass {

	public static void main(String[] args) {

		Exercise07 exercise07 = new Exercise07();

		exercise07.fizzBuzz(100);

		// de-referencing
		exercise07 = null;
	}

}
