package com.hcl.assignment07;

public class Exercise07 {

	// business logic
	public void fizzBuzz(int num) {

		for (int i = 1; i <= num; i++) {

			if (i % 3 == 0 && i % 5 == 0) {
				System.out.println(i + " :FizzBuzz");
			} else if (i % 3 == 0) {
				System.out.println(i + " :Fizz");
			} else if (i % 5 == 0) {
				System.out.println(i + " :Buzz");
			}
		}
	}
}
