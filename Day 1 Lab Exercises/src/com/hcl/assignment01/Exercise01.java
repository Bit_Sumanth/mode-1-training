package com.hcl.assignment01;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Print the following opertaions.
 */
public class Exercise01 {

	public static void main(String[] args) {

		// a. -5 + 8 * 6
		System.out.println(-5 + 8 * 6);

		// (55+9) % 9
		System.out.println((55 + 9) % 9);

		// 20 + -3*5 / 8
		System.out.println(20 + -3 * 5 / 8);

		// 5 + 15 / 3 * 2 - 8 % 3 
		System.out.println(5 + 15 / 3 * 2 - 8 % 3);

	}

}
