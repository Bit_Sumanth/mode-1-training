package com.hcl.assignment03;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         To find average of three numbers.?
 */
public class Exercise03MainClass {

	public static void main(String[] args) {

		// Object creation of the class.
		Exercise03 exercise03 = new Exercise03();

		double answer = exercise03.averageOfThreeNumbers(10, 20, 30);

		System.out.println(answer);// displays output.

		// de-referencing
		exercise03 = null;
	}

}
