package com.hcl.assignment08;

import java.util.Scanner;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         calculate the sum of odd digits.?
 */
public class UserMainCode {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter a positive number :");

		int num = scanner.nextInt();

		int ans = checkSum(num);
		if (ans == 1)
			System.out.println("Sum of odd digits is : odd");
		else
			System.out.println("Sum of odd digits is : even");
		
		scanner.close();
	}

	public static int checkSum(int num) {
		int ans = 0;
		// declare variables
		int lastDigit = 0;
		int oddDigitSum = 0;

		// loop to repeat the process
		while (num != 0) {

			// find last digit
			lastDigit = num % 10;

			// check last digit odd?
			if (lastDigit % 2 != 0) {
				// add it to sum
				oddDigitSum += lastDigit;
			}

			// remove last digit of number
			num = num / 10;
		}

		// return sum value
		if ((oddDigitSum / 3) == 0) 
			ans = 1;
		 else 
			ans = -1;
		

		return ans;

	}
}
