package com.hcl.assignment06;

import java.util.Scanner;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         ASCII Value of given character.?
 */
public class Exercise06MainClass {

	public static void main(String[] args) {

		// Instantiating
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter only a single character.");
		char character = scanner.next().charAt(0); // to read a single character from user.

		System.out.println("The ascii value of given " + character + " " + asciiValue(character));

		scanner.close();
	}

	// Business logic
	public static int asciiValue(char charValue) {

		return (int) charValue;
	}
}
