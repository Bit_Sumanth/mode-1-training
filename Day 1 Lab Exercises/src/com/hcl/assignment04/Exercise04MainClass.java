package com.hcl.assignment04;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Swapping two variables.?
 */
public class Exercise04MainClass {

	public static void main(String[] args) {

		int num1 = 10;
		int num2 = 20;

		Exercise04 exercise04 = new Exercise04();
		exercise04.swapTwoNumbers(num1, num2);

		// de-referncing
		exercise04 = null;

	}

}
