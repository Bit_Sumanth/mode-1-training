package com.hcl.assignment04;

public class Exercise04 {

	// Considering the two variables as int data type.

	public void swapTwoNumbers(int num1, int num2) {
		System.out.println("Before swapping " + num1 + " " + num2);
		int temp = num1;
		num1 = num2;
		num2 = temp;
		System.out.println("After swapping " + num1 + " " + num2);
	}
}
