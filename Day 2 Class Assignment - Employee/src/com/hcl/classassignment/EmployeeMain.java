package com.hcl.classassignment;

/**
 * 
 * @author Sumanth Babu.
 *
 */
public class EmployeeMain {

	public static void main(String[] args) {

		// Task 1 Testing
		Employee[] employeeArray = new Employee[3];
		Employee employee1 = new Employee();
		employee1.salary = 10000.F;
		employeeArray[0] = employee1;
		Employee employee2 = new Employee();
		employee2.salary = 20000.F;
		employeeArray[1] = employee2;
		Employee employee3 = new Employee();
		employee3.salary = 30000.F;
		employeeArray[2] = employee3;

		EmployeeService employeeService = new EmployeeService();
		System.out.println(employeeService.sumOfEmployeeSalaries(employeeArray));

		// Task 2 Testing
		employee1.firstName = "Hello";
		employee1.secondName = "World";
		employee2.firstName = "Good";
		employee2.secondName = "Afternoon";
		employee3.firstName = "Happy";
		employee3.secondName = "Ugadi";

		Employee emp1 = employeeService.searchByFirstName(employeeArray, "Good");
		System.out.println(emp1);

		// Task 3 Testing
		String str = employeeService.searchByFirstNameAndCount(employeeArray, "Hello");
		System.out.println(str);

		// Task 4 Testing

		String[] empNames = employeeService.getAllEmployeeNames(employeeArray);
		for (String ele : empNames) {
			System.out.println(ele);
		}
		
		// de referencing
		employee1 = null; 
		employee2 = null;
		employee3 = null;
	}

}
