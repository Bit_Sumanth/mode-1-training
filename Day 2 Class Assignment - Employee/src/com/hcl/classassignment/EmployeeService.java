package com.hcl.classassignment;

/**
 * 
 * @author Sumanth Babu.
 *
 */
public class EmployeeService {

	// Assignment Tasks.
	// 1. total sum of three employees.

	float sumOfEmployeeSalaries(Employee[] employees) {
		float totalSalaries = 0;
		for (Employee ele : employees) {
			totalSalaries += ele.salary;
		}
		return totalSalaries;
	}

	// 2. Search Employees by firstName in Employee Array

	Employee searchByFirstName(Employee[] employees, String firstName) {
		for (Employee ele : employees) {
			if (ele.firstName.equals(firstName)) {
				return ele;
			}
		}

		return null;

	}

	// 3. Search Employees by FirstName in Employee Array and count the occurrences.

	String searchByFirstNameAndCount(Employee[] employees, String firstName) {
		int count = 0;
		for (Employee ele : employees) {

			if (ele.firstName.equals(firstName)) {
				count++;
			}
		}

		return firstName + " occured " + count + " times. ";

	}

	// Task 4. To get All Employee Names.

	String[] getAllEmployeeNames(Employee[] employees) {
		String[] empNames = new String[employees.length];
		for (int i = 0; i < employees.length; i++) {
			empNames[i] = employees[i].firstName + " " + employees[i].secondName;
		}

		return empNames;

	}

}
