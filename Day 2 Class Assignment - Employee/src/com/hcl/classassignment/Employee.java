package com.hcl.classassignment;

/**
 * 
 * @author Sumanth Babu.
 *
 */
public class Employee {
	
	int empNo;
	String firstName;
	String secondName;
	float salary;

	@Override
	public String toString() {
		return "Employee [empNo=" + empNo + ", firstName=" + firstName + ", secondName=" + secondName + ", salary="
				+ salary + "]";
	}

}
