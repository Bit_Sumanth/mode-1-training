package com.hcl.assignment04;

import com.hcl.assignment03.DemoThread1;

/**
 * 
 * @author Sumanth Babu.
 * 
 * 
 *         Rewrite the Exercise03 program so that, now the class DemoThread1
 *         instead of implementing from Runnable interface, will now extend from
 *         Thread class.
 *
 */
public class Exercise04MainClass {

	public static void main(String[] args) {

		// Objects of demoThread Class
		DemoThread1 demoThread1 = new DemoThread1();

		DemoThread1 demoThread2 = new DemoThread1();

		DemoThread1 demoThread3 = new DemoThread1();

		// Creating Thread Objects and passing demoThread class objects to constructor
		// of Thread Class
		Thread thread1 = new Thread(demoThread1);
		Thread thread2 = new Thread(demoThread2);
		Thread thread3 = new Thread(demoThread3);
		
		System.out.println(thread1.getId());
		System.out.println(thread1.getName());
		
		
		// Starting the thread (Enters into Runnable State) and invokes the run method
		// business logic of respective Threads
		thread1.start();
		thread2.start();
		thread3.start();

		// de-referencing
		demoThread1 = null;
		demoThread2 = null;
		demoThread3 = null;

		thread1 = null;
		thread2 = null;
		thread3 = null;

	}

}
