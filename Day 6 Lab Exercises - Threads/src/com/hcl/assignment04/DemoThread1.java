package com.hcl.assignment04;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Rewrite the Exersice03 program so that, now the class DemoThread1
 *         instead of implementing from Runnable interface, will now extend from
 *         Thread class.
 */
public class DemoThread1 extends Thread {

	// constructor
	public DemoThread1() {
		// creating the Thread Object.
		Thread thread = new Thread();// new state
		thread.start();// Runnable state
	}

	@Override
	public void run() {

		for (int i = 1; i <= 10; i++) {
			System.out.println("running child Thread in loop : " + i);
			try {
				Thread.sleep(2000);// sleep for 2 seconds
			} catch (InterruptedException e) {// Checked Exception
				e.printStackTrace();
			}
		}

	}

}
