package com.hcl.assignment01;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a program to assign the current thread to t1. Change the name
 *         of the thread to MyThread. Display the changed name of the thread.
 *         Also it should display the current time. Put the thread to sleep for
 *         10 seconds and display the time again.
 */
public class ThreadMainClass {

	public static void main(String[] args) {

		// Creating the Thread Object
		ThreadExample threadExample = new ThreadExample();

		threadExample.start();// Here thread is in runnable state.

		// Name of the Thread before chainging its value.
		System.out.println("Thread Name before chainging : " + threadExample.getName());

		// Name of the Thread after chainging its value.
		threadExample.setName("MyThread");
		System.out.println("Thread Name after chainging : " + threadExample.getName());

		// de-referincing
		threadExample = null;
	}

}
