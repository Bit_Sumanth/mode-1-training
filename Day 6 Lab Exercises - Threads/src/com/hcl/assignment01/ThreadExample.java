package com.hcl.assignment01;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a program to assign the current thread to t1. Change the name
 *         of the thread to MyThread. Display the changed name of the thread.
 *         Also it should display the current time. Put the thread to sleep for
 *         10 seconds and display the time again.
 */

import java.time.LocalTime;

public class ThreadExample extends Thread {

	@Override
	public void run() {

		// Returns a reference to the currently executing thread object.
		@SuppressWarnings("unused")
		Thread t1 = Thread.currentThread();

		LocalTime currentTime = LocalTime.now();
		System.out.println("Current Time before sleep : " + currentTime);// displays current time.

		try {
			// sleep for 10 seconds
			Thread.sleep(10000);// static method in Thread Class used to interupt the flow.
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("Current Time after the sleep : " + currentTime);// displays current time.
	}

}
