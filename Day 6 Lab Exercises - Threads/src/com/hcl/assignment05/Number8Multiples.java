package com.hcl.assignment05;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a program to create a class Number  which implements Runnable.
 *         Run method displays the multiples of a number accepted as a
 *         parameter. In main create three objects - first object should display
 *         the multiples of 2, second should display the multiples of 5 and
 *         third should display the multiples of 8. Display appropriate message
 *         at the beginning and ending of thread. The main thread should wait
 *         for the first object to complete. Display the status of threads
 *         before the multiples are displayed and after completing the multiples.?
 */
public class Number8Multiples implements Runnable {

	// variable
	private int num;

	// arg-constructor
	public Number8Multiples(int num) {
		this.num = num;

	}

	Thread.State threadState = Thread.currentThread().getState();

	@Override
	public void run() {

		System.out.println("Multiples of Thread 8 Started : ");

		System.out.println(
				"Thread State Before 8  Multiples : " + Thread.currentThread().getName() + " : " + threadState);

		for (int i = 1; i <= this.num; i++) {
			if ((i % 8) == 0) {
				System.out.println("Multiples of 8 : " + i);
			}
		}

		System.out.println("Multiples of Thread 8 Ended : ");

		System.out.println(
				"Thread State After the 8 Multiples : " + Thread.currentThread().getName() + " : " + threadState);
	}

	// getters and setters
	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

}
