package com.hcl.assignment05;

import java.util.Scanner;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a program to create a class Number  which implements Runnable.
 *         Run method displays the multiples of a number accepted as a
 *         parameter. In main create three objects - first object should display
 *         the multiples of 2, second should display the multiples of 5 and
 *         third should display the multiples of 8. Display appropriate message
 *         at the beginning and ending of thread. The main thread should wait
 *         for the first object to complete. Display the status of threads
 *         before the multiples are displayed and after completing the
 *         multiples.?
 */
public class Exercise05MainClass {

	public static void main(String[] args) {

		System.out.println("Enter a positive number");

		Scanner scanner = new Scanner(System.in);

		int input = scanner.nextInt();

		if (input <= 0)
			System.out.println("Enter a valid Number : ");

		// Objects of NumberThread Class
		Number2Multiples number2Thread1 = new Number2Multiples(input);

		Number5Multiples number5Thread2 = new Number5Multiples(input);

		Number8Multiples number8Thread3 = new Number8Multiples(input);

		// closing the costly resources.
		scanner.close();

		// Creating Thread Objects and passing NumberThread class objects to constructor
		// of Thread Class
		Thread thread1 = new Thread(number2Thread1);
		Thread thread2 = new Thread(number5Thread2);
		Thread thread3 = new Thread(number8Thread3);

		// Starting the thread (Enters into Runnable State) and invokes the run method
		// business logic of respective Threads
		// thread1.start();
		// thread2.start();
		// thread3.start();

		// There is no context switching between the Threads. runs one after the other.
		thread1.run();
		thread2.run();
		thread3.run();

		// de-referencing

		number2Thread1 = null;
		number5Thread2 = null;
		number8Thread3 = null;

		thread1 = null;
		thread2 = null;
		thread3 = null;

	}

}
