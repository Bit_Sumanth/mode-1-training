package com.hcl.assignment03;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a program to create a class DemoThread1 implementing Runnable
 *         interface. In the constructor, create a new thread and start the
 *         thread. In run() display a message "running child Thread in loop : "
 *         display the value of the counter ranging from 1 to 10. Within the
 *         loop put the thread to sleep for 2 seconds. In main create 3 objects
 *         of the DemoTread1 and execute the program
 * 
 */
public class Exercise03MainClass {

	public static void main(String[] args) {

		// Objects of demoThread Class
		DemoThread1 demoThread1 = new DemoThread1();

		DemoThread1 demoThread2 = new DemoThread1();

		DemoThread1 demoThread3 = new DemoThread1();

		// Creating Thread Objects and passing demoThread class objects to constructor
		// of Thread Class
		Thread thread1 = new Thread(demoThread1);
		Thread thread2 = new Thread(demoThread2);
		Thread thread3 = new Thread(demoThread3);

		// Starting the thread (Enters into Runnable State) and invokes the run method
		// business logic of respective Threads
		thread1.start();
		thread2.start();
		thread3.start();

		// de-referencing
		demoThread1 = null;
		demoThread2 = null;
		demoThread3 = null;

		thread1 = null;
		thread2 = null;
		thread3 = null;
	}

}
