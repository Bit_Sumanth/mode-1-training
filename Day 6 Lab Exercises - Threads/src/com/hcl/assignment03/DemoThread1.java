package com.hcl.assignment03;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         Write a program to create a class DemoThread1 implementing Runnable
 *         interface. In the constructor, create a new thread and start the
 *         thread. In run() display a message "running child Thread in loop : "
 *         display the value of the counter ranging from 1 to 10. Within the
 *         loop put the thread to sleep for 2 seconds. In main create 3 objects
 *         of the DemoTread1 and execute the program
 * 
 */
public class DemoThread1 implements Runnable {

	// constructor
	public DemoThread1() {
		// creating the Thread Object.
		Thread thread = new Thread();// new state
		thread.start();// Runnable state
	}

	@Override
	public void run() {

		for (int i = 1; i <= 10; i++) {
			System.out.println("running child Thread in loop : " + i);
			try {
				Thread.sleep(2000);// sleep for 2 seconds
			} catch (InterruptedException e) {// Checked Exception
				e.printStackTrace();
			}
		}

	}

}
