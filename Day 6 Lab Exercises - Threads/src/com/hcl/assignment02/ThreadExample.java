package com.hcl.assignment02;

import java.time.LocalTime;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         remove the try{}catch(){} block surrounding the sleep method and try
 *         to execute the code. What is your observation?
 */
public class ThreadExample extends Thread {

	@Override
	public void run() {

		// Returns a reference to the currently executing thread object.
		@SuppressWarnings("unused")
		Thread t1 = Thread.currentThread();

		LocalTime myObj = LocalTime.now();
		System.out.println("Current Time before sleep : " + myObj);// displays current time.

		/*
		 * It is a Checked Exception we can handle this either by surrounding with
		 * try{}-catch() block{} or thorowing exception to JVM without handling.
		 */
		// Thread.sleep(10000);

		System.out.println("Current Time after the sleep : " + myObj);// displays current time.

	}

}
