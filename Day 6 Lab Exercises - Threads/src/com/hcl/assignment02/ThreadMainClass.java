package com.hcl.assignment02;

/**
 * 
 * @author Sumanth Babu.
 * 
 *         remove the try{}catch(){} block surrounding the sleep method and try
 *         to execute the code. What is your observation?
 */
public class ThreadMainClass {

	public static void main(String[] args) {

		// Thread Object
		ThreadExample threadExample = new ThreadExample();

		// starting the Thread.
		threadExample.start();

		// de-referencing
		threadExample = null;
	}

}
